﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace РВИП2_консоль
{
    class Program
    {
        // одновременно собираются 2 машины

        const int Maxcount_car = 10;
        public int count_car = 0;

        static void Main(string[] args)
        {
            Thread th_car1 = new Thread(ThreadFunction);
            th_car1.Start(true);
            Thread th_car2 = new Thread(ThreadFunction);
            th_car2.Start(false);
            Console.Read();
        }

        static void ThreadFunction(Object input)
        {
            bool flag = (bool)input;
            Transporter transp = new Transporter();

            for (int i = 0; i < Maxcount_car; i++)
            {
                //Если входящий флаг true - значит "я первый поток"
                if (flag)
                {
                    transp.Constructor();
                    Console.WriteLine("Собрал модуль в первом потоке");
                }
                //Если входящий флаг false - значит "я второй поток"
                else
                {
                    transp.Constructor();
                    Console.WriteLine("Собрал модуль во втором потоке");
                }

            }
        }

    }
}
